package com.ayushbagaria.cardgame;


import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements ImageAdapter.GridItemClickListener{

    int column = 3 ;
    GridView gridView;
    static  String[] str_arr ;
    int displayWidth,displayHeight,required_height;
    Handler myHandler;
    ArrayList<Integer> list;
    ArrayList<GridCell> cardList;
    int counter = 0 , selected = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        displayWidth = metrics.widthPixels ;
        displayHeight = metrics.heightPixels;
        required_height = displayHeight ;
        gridView = (GridView) findViewById(R.id.gridView1);
        gridView.setNumColumns(column);
        int arrSize = column*column ;
        list = new ArrayList<Integer>();
        cardList = new ArrayList<GridCell>();
        for (int i=0; i<52; i++) {
            list.add(new Integer(i));
            if (i <=9) {
                GridCell myCell;
                myCell = new GridCell(i,0);
                cardList.add(myCell);
            }
        }
        Collections.shuffle(list);
        for (int i=0; i<8; i++) {
            cardList.get(i).setValue(list.get(i));
        }
        cardList.get(8).setValue(list.get(7));
        Collections.shuffle(cardList);
        int column_width = displayWidth/column ;
        int column_height = required_height/column ;
        gridView.setAdapter(new ImageAdapter(this, arrSize, column_width, column_height, cardList));
        myHandler = new Handler(this.getMainLooper());
    }

    @Override
    public void onResume() {
        super.onResume();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final int size = gridView.getChildCount();
                for(int i = 0; i < size; i++) {
                    ViewGroup gridChild = (ViewGroup) gridView.getChildAt(i);
                    int childSize = gridChild.getChildCount();
                    for(int k = 0; k < childSize; k++) {
                        if( gridChild.getChildAt(k) instanceof ViewFlipper) {
                            ((ViewFlipper) gridChild.getChildAt(k)).setDisplayedChild(1);;
                        }
                    }
                }
            }
            }, 5500);
    }

    @Override
    public void onItemClick(int position) {
       flipCardBack(position);
      if (selected ==-1 && (cardList.get(position).getId() == 8 || cardList.get(position).getId() == 7 )) {
            selected = cardList.get(position).getId();
            Toast.makeText(
                    getApplicationContext(),
                    "You have selected the right one, now find the second one", Toast.LENGTH_SHORT).show();
        } else if (selected == 8 &&   cardList.get(position).getId() == 7 ){
            Toast.makeText(
                    getApplicationContext(),
                    "You have won", Toast.LENGTH_SHORT).show();
            selected = -1;
            counter = 0;
        } else if (selected == 7 &&  cardList.get(position).getId() == 8 ){
            Toast.makeText(
                    getApplicationContext(),
                    "You have won", Toast.LENGTH_SHORT).show();
            selected = -1;
            counter = 0;
        }  else if (counter == 1) {
            Toast.makeText(
                    getApplicationContext(),
                    "You have lost", Toast.LENGTH_SHORT).show();
            selected = -1;
            counter = 0;
        } else {
            counter++;
            Toast.makeText(
                    getApplicationContext(),
                    "You have selected the wrong one, last chance left", Toast.LENGTH_SHORT).show();
        }
    }

    public void flipCardBack(final int position) {
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final int size = gridView.getChildCount();
                    ViewGroup gridChild = (ViewGroup) gridView.getChildAt(position);
                    int childSize = gridChild.getChildCount();
                    for(int k = 0; k < childSize; k++) {
                        if( gridChild.getChildAt(k) instanceof ViewFlipper) {
                            ((ViewFlipper) gridChild.getChildAt(k)).setDisplayedChild(1);;
                        }
                    }

            }
        }, 1500);
    }
}
