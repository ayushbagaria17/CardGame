package com.ayushbagaria.cardgame;


public class GridCell {
    private int value;
    private int id;

    public GridCell () {}

    public GridCell (int id, int value) {
        this.id = id;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
