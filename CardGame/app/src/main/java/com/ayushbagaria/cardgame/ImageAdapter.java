package com.ayushbagaria.cardgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;
import java.util.ArrayList;


public class ImageAdapter extends BaseAdapter {
    private Context context;
    private final int size;
    int column_width, column_height;
    ArrayList<GridCell> cardList;
    GridItemClickListener mlistener;
    public ImageAdapter(Context context, int size, int column_width, int column_height, ArrayList<GridCell> cardList) {
        this.context = context;
        this.size = size;
        this.column_width = column_width;
        this.column_height = column_height;
        this.cardList = cardList;
        this.mlistener = (GridItemClickListener) context;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_item, null);
            viewHolder.frontImageView = (ImageView) convertView
                    .findViewById(R.id.pictureFront);

            viewHolder.backImageView = (ImageView) convertView
                    .findViewById(R.id.pictureBack);

            viewHolder.viewFlipper = (ViewFlipper) convertView.findViewById(R.id.my_view_flipper);

            viewHolder.lin = (LinearLayout) convertView.findViewById(R.id.lin);
            convertView.setTag(viewHolder);
            android.widget.AbsListView.LayoutParams parms = new android.widget.AbsListView.LayoutParams(column_width, column_height);
            viewHolder.lin.setLayoutParams(parms);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Bitmap m_d = BitmapFactory.decodeResource(context.getResources(),
                Constants.cardsArray[cardList.get(position).getValue()]);
        if (m_d != null) {
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(m_d, column_width, column_height, true);
            viewHolder.frontImageView.setImageBitmap(resizedBitmap);
        }
        m_d = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.red_joker);
        if (m_d != null) {
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(m_d, column_width, column_height, true);
            viewHolder.backImageView.setImageBitmap(resizedBitmap);
        }
        viewHolder.viewFlipper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View click) {

                flipViewFlipper(viewHolder.viewFlipper);
                mlistener.onItemClick(position);

            }



        });
        return convertView;
    }
    private void flipViewFlipper(ViewFlipper flipper){
        if(flipper.getDisplayedChild() == 0){
            flipper.setDisplayedChild(1);
        }
        else{
           flipper.setDisplayedChild(0);
        }

    }
    public interface GridItemClickListener  {
        public  void onItemClick(int position);
    }
    @Override
    public int getCount() {
        return size;
    }

    public class ViewHolder {
        ImageView backImageView;
        ImageView frontImageView;
        LinearLayout lin;
        ViewFlipper viewFlipper;

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}


